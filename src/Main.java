import java.util.HashMap;

public class Main {
  static TrapezeIntegration[] integrals = {
      new TrapezeIntegration(x -> x),
      new TrapezeIntegration(x -> x*x),
      new TrapezeIntegration(x -> (x*x + 10) / (x + 1.0)),
      new TrapezeIntegration(x -> (x*x*x) / (Math.sin(x))),
      new TrapezeIntegration(x -> (x*x + 2*x + 10) / (8*x - 15))
  };

  public static void main(String[] args) {
    String[] menu = {
        "∫ xdx",
        "∫ x²dx",
        "∫ (x²+10)dx/(x+1)",
        "∫ x³dx/sin(x)",
        "∫ (x²+2x+10)dx/(8x -15)",
        "Exit."
    };
    UserInterface ui = new UserInterface(menu);
    TrapezeIntegration integrator = null;
    while (true) {
      ui.printMenu();
      int choice = ui.getChoice();
      if (1 <= choice && choice <= 5) {
        integrator = integrals[choice -1];
      } else if (choice == 6) {
        System.out.println("Goodbye!");
        System.exit(0);
      } else {
        System.exit(-1);
      }
      Double a = getArrayBorder("first");
      Double b = getArrayBorder("second");
      Double accuracy = getAccuracy();
      HashMap<String, Double> map = integrator.integrate(a, b, accuracy);
      PrintResult(map);
    }
  }

  public static Double getArrayBorder(String order) {
    return UserInterface.getDouble(
        "Enter the " + order + " part of the interval",
        "It should be a number. Try again."
    );
  }
  public static void PrintResult(HashMap<String, Double> map) {
    System.out.println(String.format("Integration result: %s", map.get("integral")));
    System.out.println(String.format("Amount of steps: %s", map.get("steps")));
    System.out.println(String.format("Reached accuracy: %s", map.get("accuracy")));
  }
  public static Double getAccuracy() {
    Double accuracy = 0.0;
    System.out.println("Enter the accuracy you want to reach");
    while (Math.abs(accuracy) <= 0.00001) {
      System.out.println("Accuracy may not be less than 10e-5");
      accuracy = UserInterface.getDouble();
    }
    return accuracy;
  }
}
