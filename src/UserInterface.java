import java.util.Locale;
import java.util.Scanner;

public class UserInterface {
  private String[] menu;

  public UserInterface(String[] menu) {
    this.menu = menu;
  }

  public void printMenu() {
    for (int i = 0; i < menu.length; ++i) {
      print(String.format("%s. %s", i + 1, menu[i]));
    }
  }

  public int getChoice() {
    int choice;
    Scanner scanner = new Scanner(System.in);
    while (true) {
      try {
        choice = scanner.nextInt();
        if (0 < choice && choice <= menu.length) {
          return choice;
        } else {
          print("No such menu option. Try again.");
          printMenu();
        }
      } catch (Exception e) {
        scanner.nextLine();
        print("No such menu option. Try again.");
        printMenu();
      }
    }
  }

  static String getLine() {
    Scanner scanner = new Scanner(System.in);
    return scanner.nextLine();
  }

  static int getInt(String message, String errMessage) {
    Scanner scanner = new Scanner(System.in).useLocale(Locale.US);;
    print(message);
    while (true) {
      try {
        return scanner.nextInt();
      } catch (Exception e) {
        scanner.nextLine();
        print(errMessage);
      }
    }
  }

  static Double getDouble() {
    Scanner scanner = new Scanner(System.in).useLocale(Locale.US);;
    while (true) {
      try {
        return scanner.nextDouble();
      } catch (Exception e) {
        scanner.nextLine();
        System.out.println("Woops. That's not a valid number");
      }
    }
  }

  static Double getDouble(String message, String errMessage) {
    Scanner scanner = new Scanner(System.in).useLocale(Locale.US);;
    print(message);
    while (true) {
      try {
        return scanner.nextDouble();
      } catch (Exception e) {
        scanner.nextLine();
        print(errMessage);
      }
    }
  }

  static void print(Object obj) {
    System.out.println(obj.toString());
  }
}
