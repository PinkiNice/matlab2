import java.util.Dictionary;
import java.util.HashMap;
import java.util.function.Function;

public class TrapezeIntegration {
  static private Double k = 1.0 / 3.0;
  static private Integer initialSteps = 10;
  private Function<Double, Double> fun; //Function that will be integrated

  public TrapezeIntegration(Function<Double, Double> functionToIntegrate) {
    this.fun = functionToIntegrate;
  }

  public HashMap<String, Double> integrate(Double a, Double b, Double accuracy) {
    Integer mp = 1; // multiplier of the integral (to change signs)
    HashMap<String, Double> map = new HashMap<String, Double>();
    if (a.equals(b)) {
      map.put("integral", 0.0);
      map.put("accuracy", 0.0);
      map.put("steps", 0.0);
    } else if (a > b) { //swap borders
      Double temp = a;
      a = b;
      b = temp;
      mp = -1; //multiplier set to -1 because we swapped the integral borders
    }

    Integer n = initialSteps; // Amount of steps for first iteration
    Double result = count(a, b, n) * mp; //first iteration result
    Boolean accuracyReached = false; //set to true when we reach needed accuracy
    Double currentAccuracy = 0.0;
    while (!accuracyReached) {
      n *= 2; // Double the amount of steps
      Double currentResult = count(a, b, n) * mp;
      currentAccuracy = k * Math.abs(result - currentResult);
      if (currentAccuracy <= accuracy) {
        accuracyReached = true;
      }
      result = currentResult;
    }
    map.put("integral", result);
    map.put("accuracy", currentAccuracy);
    map.put("steps", new Double(n));
    return map;
  }

  /**
   * @param a lowest border of integral
   * @param b highest border of the integral
   * @param n number of steps the interval will be divided by
   * @return square
   */
  private Double count(Double a, Double b, Integer n) {
    Double step;
    Double integral = 0.0;
    step = (b - a) / n;
    for (int i = 1;  i < n; ++i) {
      integral += this.fun.apply(a + step * i); // find the function in every anchor point
    }
    integral += (this.fun.apply(a) + this.fun.apply(b)) / 2.0; // add function at the borders
    integral *= step; // multiply it by the step
    return integral;
  }
}
